# -*- coding: utf-8 -*-
from django.db import models


class Kingdom(models.Model):
    label = models.CharField(unique=True, max_length=64)

    def __str__(self):
        return self.label


class Specie(models.Model):
    label = models.CharField(unique=True, max_length=64)

    def __str__(self):
        return self.label


class Entry(models.Model):
    access_id = models.CharField(max_length=25, unique=True)
    kingdom = models.ForeignKey(Kingdom)
    specie = models.ForeignKey(Specie)
    sequence = models.TextField(max_length=128)

    def __str__(self):
        return '{} - {} - {}'.format(self.access_id, self.kingdom, self.specie)
