# -*- encode: utf-8 -*-
from core.models import Entry, Kingdom, Specie
from core.apps import EntrySerializer, KingdomSerializer, SpecieSerializer
from rest_framework import viewsets, filters


class EntryViewSet(viewsets.ModelViewSet):
    queryset = Entry.objects.all()
    serializer_class = EntrySerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filter_fields = ('kingdom', 'kingdom__label', 'specie', 'specie__label')


class KingdomViewSet(viewsets.ModelViewSet):
    queryset = Kingdom.objects.all()
    serializer_class = KingdomSerializer
    filter_fields = ('label',)


class SpecieViewSet(viewsets.ModelViewSet):
    queryset = Specie.objects.all()
    serializer_class = SpecieSerializer
    filter_fields = ('label',)
