# -*- coding: utf-8 -*-
import gzip
import os
import time

import math
import requests

from django.core.management.base import BaseCommand, CommandError
from django.db import IntegrityError, transaction

from core.models import Entry, Kingdom, Specie


ROOT_DOWNLOAD = '/tmp'


class Command(BaseCommand):
    help = 'Download, unpack and import the first 5 thousands records'

    def __init__(self):
        super(Command, self).__init__()
        self.total_entries_to_save = 5000

        self.kingdom_dict = {}
        for k in Kingdom.objects.all():
            self.kingdom_dict[k.label] = k

        self.specie_dict = {}
        for s in Specie.objects.all():
            self.specie_dict[s.label] = s

    def add_arguments(self, parser):
        parser.add_argument('-file_uri', nargs='+', type=str)
        parser.add_argument('-file_path', nargs='+', type=str)
        parser.add_argument('-total', type=int)
        parser.add_argument('--save_all',
                            action='store_true',
                            default=False,
                            help='Save all Entries')

    def handle(self, *args, **options):
        if options.get('total'):
            self.total_entries_to_save = options.get('total')
        elif options.get('save_all'):
            self.total_entries_to_save = float('+Infinity')

        fpath_list = []

        # download the .gz files
        if options['file_uri']:
            for uri in options['file_uri']:
                fp = self.download_file(uri, ROOT_DOWNLOAD)
                fpath_list.append(fp)

        if options['file_path']:
            fpath_list.extend(options['file_path'])

        if fpath_list:
            for f_path in fpath_list:
                # if the file is gzip format them extract the data
                if '.gz' in f_path:
                    fname = self.gzip_extract(f_path)
                else:
                    fname = f_path

                # read a .fasta file, get the Entries and save it
                self.extract_entries_and_save(fname)
        else:
            self.stdout.write(self.style.ERROR('No valid file_path or file_uri found'))

        return None

    def download_file(self, uri, destination):
        """ Download a file from and uri and save it on destination folder """
        self.stdout.write('Start downloading: %s' % uri)

        response = requests.get(uri, stream=True)
        if not response.ok:
            self.stdout.write('Was not possible to access the uri %s' % uri)
            return None

        fname = os.path.join(destination, uri.split('/')[-1])
        total_fsize = int(response.headers.get('Content-Length'))
        current_download_fsize = total_fsize
        chunk_size = 1024
        amount_download_status_to_print = 10

        with open(fname, 'wb') as f:
            for chunk in response.iter_content(chunk_size=chunk_size):
                if chunk:
                    f.write(chunk)

                    # print the download status
                    current_download_fsize -= chunk_size
                    if current_download_fsize <= (total_fsize / 10) * amount_download_status_to_print:
                        self.stdout.write('downloading: %s...' % (self.convert_size(current_download_fsize)))
                        amount_download_status_to_print -= 1

        self.stdout.write(self.style.SUCCESS('Download finished.'))

        return fname

    def gzip_extract(self, gz_fname):
        """ Extract a gzip file"""
        self.stdout.write('\nStart Extracting...')

        with gzip.open(gz_fname, 'rb') as f_in, open(gz_fname[:-3], 'wb') as f_out:
            for line in f_in:
                f_out.write(line)

        self.stdout.write(self.style.SUCCESS('File successfully extracted on: %s' % f_out.name))
        return os.path.realpath(f_out.name)

    @transaction.atomic
    def extract_entries_and_save(self, fname):
        """ Extract the entries from a file and save them on DB """
        self.stdout.write('\nStart saving Entries...')

        time_start = time.time()
        count = 0

        with open(fname) as f:
            for line in f:
                if line.startswith('>'):
                    # extract the data from a line
                    info_list = line[1:][:-1].split(';')
                    access_id, kingdom_name = info_list[0].split(' ')
                    specie_name = info_list[-1]
                    sequence = f.readline()[:-1]

                    # create the objects
                    try:
                        with transaction.atomic():
                            if not self.kingdom_dict.get(kingdom_name):
                                self.kingdom_dict[kingdom_name] = Kingdom.objects.create(label=kingdom_name)

                            if not self.specie_dict.get(specie_name):
                                self.specie_dict[specie_name] = Specie.objects.create(label=specie_name)

                            Entry.objects.create(access_id=access_id,
                                                 kingdom=self.kingdom_dict[kingdom_name],
                                                 specie=self.specie_dict[specie_name],
                                                 sequence=sequence)
                    except IntegrityError:
                        continue

                    # stop condition
                    count += 1
                    if count >= self.total_entries_to_save:
                        break

        time_total = time.time() - time_start

        self.stdout.write(self.style.SUCCESS('Save %s Entries in %s seconds\nAverage of %s entries/s' % (count,
                                                                                                         time_total,
                                                                                                         count/time_total)))
        return None

    @staticmethod
    def convert_size(size_bytes):
        if size_bytes <= 0:
            return '0B'

        size_name = ("B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB")
        i = int(math.floor(math.log(size_bytes, 1024)))
        p = math.pow(1024, i)
        s = round(size_bytes/p, 2)

        return '%s %s' % (s, size_name[i])
