from django.contrib import admin

from core.models import Entry, Kingdom, Specie


class KingdomInline(admin.TabularInline):
    model = Kingdom
    extra = 1


class SpecieInline(admin.TabularInline):
    model = Specie
    extra = 1


class EntryInline(admin.TabularInline):
    model = Entry
    extra = 1


@admin.register(Entry)
class EntryAdmin(admin.ModelAdmin):
    list_per_page = 1000
    fields = ('access_id', 'sequence', 'kingdom', 'specie')
    list_display = ('access_id', 'kingdom', 'specie', 'sequence')
    list_filter = ('kingdom',)
    search_fields = ('access_id', 'kingdom', 'specie')


@admin.register(Kingdom)
class KingdomAdmin(admin.ModelAdmin):
    list_per_page = 1000
    search_fields = ('label',)


@admin.register(Specie)
class SpecieAdmin(admin.ModelAdmin):
    list_per_page = 1000
    search_fields = ('label',)
