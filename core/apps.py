from core.models import Entry, Kingdom, Specie
from rest_framework import serializers


class EntrySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Entry
        fields = ('id', 'access_id', 'kingdom', 'specie', 'sequence')


class KingdomSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Kingdom
        fields = ('id', 'label')


class SpecieSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Specie
        fields = ('id', 'label')
