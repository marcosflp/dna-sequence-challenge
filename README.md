# Challenge
Application that manage dna sequence entries

## Requirements
* Python >= 3.5


## Installation and Configuration
```bash
$ git clone https://marcosflp@bitbucket.org/marcosflp/dna-sequence-challenge.git
$ cd dna-sequence-challenge
$ pip install -r requirements
$ python manage.py migrate
$ python manage.py createsuperuser
```

## Usage

### MANAGEMENT COMMANDS

The import_entries command is used to read, parse, and save entries from a .fasta file.
The default amount of entries to save is 5000.
```bash
$ python manage.py import_entries -file_uri https://www.arb-silva.de/fileadmin/silva_databases/release_128/Exports/SILVA_128_LSURef_tax_silva.fasta.gz

```

Save all entries of a file with --save_all or set the limit with -total

* --save_all
* -total 5000

**Examples:**

From a uri or list of uri's
```bash
$ python manage.py import_entries -file_uri uri1 uri2 uri3
```

From a local files
```bash
$ python manage.py import_entries -file_path fpath1 fpath2 fpath3
```

### Endpoints
http://localhost:8000/entry/
http://localhost:8000/entry?kingdom__label=Archaea

http://localhost:8000/kingdom/

http://localhost:8000/Specie/
